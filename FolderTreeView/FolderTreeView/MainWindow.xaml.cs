﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace FolderTreeView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Build the tree upto given path
        /// </summary>
        /// <param name="FolderPath">Folder path selected by user</param>
        /// <returns>Treeview for the given path</returns>
        public TreeViewItem PathTree(string FolderPath)
        {
            // split the given path to get all directories as a array
            string[] folders = FolderPath.Split('\\');

            // check whether the given path is valid 
            if (!(File.Exists(FolderPath)||Directory.Exists(FolderPath)))
            {
                MessageBox.Show("Path is not a valid file or directory.","Not found");
                return null;
            }
            TreeViewItem parent = new TreeViewItem();
            parent.Header = folders[0];
            TreeViewItem current = parent;
            parent.IsExpanded = true;
            // create directories items and add that items to previously added item
            for (int i = 1; i < folders.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(folders[i]))
                {
                    TreeViewItem child = new TreeViewItem();
                    child.Header = folders[i];
                    current.Items.Add(child);
                    current = child;
                    current.IsExpanded = true;
                }
            }
            // add the parent item to UI Treeview
            treeview1.Items.Add(parent);
            // return the last created item
            return current;
        }

        /// <summary>
        /// Add all the files present inthe given directory
        /// </summary>
        /// <param name="parent">Items in which files to be added</param>
        /// <param name="path">Path for the directory</param>
        public void AddFiles(TreeViewItem parent, string path)
        {
            string[] files;
            try
            {
                files = Directory.GetFiles(path);
                // create Items for Files and add it to Directory item
                foreach (string name in files)
                {
                    TreeViewItem c1 = new TreeViewItem();
                    c1.Header = Path.GetFileName(name);
                    parent.Items.Add(c1);
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show( "Cannot access files", "Unauthorized Access");
            }
        }

        /// <summary>
        /// Add sub directories to given directories
        /// </summary>
        /// <param name="parent">parent directory item</param>
        /// <param name="path">path of the directory</param>
        /// <returns>last created treeview item</returns>
        public TreeViewItem AddDirectories(TreeViewItem parent, string path)
        {
            string[] directories;
            try
            {
                directories = Directory.GetDirectories(path);
                // create directories and files for the given directories
                foreach (string dirname in directories)
                {
                    TreeViewItem child = new TreeViewItem();
                    child.Header = Path.GetFileName(dirname);
                    // add files inside the subdirectory
                    AddFiles(child, dirname);
                    // add directories inside the subdirectories
                    child = AddDirectories(child, dirname);
                    parent.Items.Add(child);
                }
                return parent;
            }
            catch (UnauthorizedAccessException)
            {
                return parent;
            }
        }
        
        /// <summary>
        /// Update treeview Items based on the path
        /// </summary>
        /// <param name="sender">Control element</param>
        /// <param name="e">event argument</param>
        public void GetTreeView(object sender, RoutedEventArgs e)
        {
            treeview1.Items.Clear();
            string FileName = textbox.Text;
            // build a tree upto given path
            TreeViewItem lastItem = PathTree(FileName);
            if (lastItem != null)
            {
                // add directories present in the path
                lastItem = AddDirectories(lastItem, FileName);
                //add the files present inside all diectories
                AddFiles(lastItem, FileName);
            }
        }
        /// <summary>
        /// Open dialogue box to select a folder and set path to textbox
        /// </summary>
        /// <param name="sender">Control element</param>
        /// <param name="e">event argument</param>
        public void SetFolderPath(object sender, RoutedEventArgs e)
        {
            // Open Folder dialogue box
            System.Windows.Forms.FolderBrowserDialog openFileDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            openFileDialog1.ShowDialog();

            // set the path to the textbox content
            textbox.Text = openFileDialog1.SelectedPath;
        }
    }
}
